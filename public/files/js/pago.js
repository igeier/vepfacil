var PagoMinimo = document.getElementById("pm").value
var SaldoTotal = document.getElementById("st").value

function CargaImporte(importe)
  {
    if (importe == 1) {
  document.getElementById("importe").value = PagoMinimo;
  }
  if (importe == 2) {
    document.getElementById("importe").value = SaldoTotal;
  }
  };

// 4: VISA, 51 -> 55: MasterCard, 36-38-39: DinersClub, 34-37: American Express, 65: Discover, 5019: dankort
$(function(){
  
  var cards = [{
    nome: "mastercard",
    colore: "#0061A8",
    src: "./files/img/mastercard.svg"
  }, {
    nome: "visa",
    colore: "#1a1f71",
    src: "./files/img/visa.png"
  }, {
    nome: "dinersclub",
    colore: "#888",
    src: "./files/img/diners.png"
  }, {
    nome: "americanExpress",
    colore: "#108168",
    src: "./files/img/american.png"
  }, {
    nome: "cabal",
    colore: "goldenrod",
    src: "./files/img/cabal.png"
  }];
  
  var month = 0;
  var html = document.getElementsByTagName('html')[0];
  var number = "";
  
  var selected_card = -1;
  var cvc =  document.getElementById("cvc");

  cvc.onfocus=function(){
     $(".card").css("transform", "rotatey(180deg)");
  }

cvc.onblur=function(){
     $(".card").css("transform", "rotatey(0deg)");
  }


  $("#tarjeta").keydown(function(event){

  if(event.key != "Backspace"){
    if($("#tarjeta").val().length == 4 || $("#tarjeta").val().length == 9 || $("#tarjeta").val().length == 14){
      $("#tarjeta").val($("#tarjeta").val() +  " ");
    }
    }
  });


  $("#tarjeta").keyup(function(event){

    $("#cardnumber").text($(this).val());
    number = $(this).val();

    if(parseInt(number.substring(0, 2)) >= 50 && parseInt(number.substring(0, 2)) < 56 || parseInt(number.substring(0, 4)) > 2220 && parseInt(number.substring(0, 4)) < 2721){
      selected_card = 0;
    }else if(parseInt(number.substring(0, 1)) == 4){
      selected_card = 1;  
    }else if(parseInt(number.substring(0, 2)) == 36 || parseInt(number.substring(0, 2)) == 38 || parseInt(number.substring(0, 2)) == 39){
      selected_card = 2;     
    }else if(parseInt(number.substring(0, 2)) == 34 || parseInt(number.substring(0, 2)) == 37){
      selected_card = 3; 
    }else if(parseInt(number.substring(0, 2)) == 58 || parseInt(number.substring(0, 3)) == 604){
      selected_card = 4; 
    }else{
      selected_card = -1; 
    }
    
    if(selected_card != -1){
      html.setAttribute("style", "--card-color: " + cards[selected_card].colore);  
      $("#cardlogo").attr("src", cards[selected_card].src).show();
    }else{
      html.setAttribute("style", "--card-color: #B9B9B9");
      $("#cardlogo").attr("src", "").hide();
    }
    
    if($(".cardnumber").text().length === 0){
      $(".cardnumber").html("&#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF;");
    }

  });

  $("#nombre").keyup(function(){  
    $("#name").text($(this).val());  
    if($("#nombre").val().length === 0){
        $("#name").text("Nombre completo");
    }
    return event.charCode;
  });
  
  $("#cvc").keyup(function(){
    $("#code").text($(this).val());
    if($(this).val().length === 0){
      $("#code").text("Código de seguridad");
    }
  });

  $("#vencim").keyup(function(event){
    $("#vtom").text($(this).val());
    if($("#vencim").val().length === 0){
          $("#vtom").text("MM");
      }
      return event.charCode;
  });

  //Date expire input month
  $("#vencia").keyup(function(event){
    $("#vtoa").text($(this).val());
    if($("#vencia").val().length === 0){
          $("#vtoa").text("AA");
      }
      return event.charCode;
  });
});

var newcard = document.getElementById("newcard");
var newc = document.getElementById("new");

$("input[type=radio]").on( 'change', function() {
    if( $(newc).prop('checked') ) {
        newcard.style.display = "block" && $(newcard).slideDown("slow");
    } else {
        newcard.style.display = "none" && $(newcard).slideUp("slow");
    }
});