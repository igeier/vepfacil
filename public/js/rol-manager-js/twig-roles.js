
$('#table-roles').DataTable({
    "columnDefs": [ {
        "orderable": false,
        "targets": [3]
    }],
    "order": [
        [0, 'asc']
    ],
    "displayLength": 10,
    "searching": false,
    "info": false, 
    "lengthChange": false,
    "language": {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    }        
});

function resetValue(){
    $("#rol-code").val("");
    $("#rol-name").val("");
    $("#rol-description").val("");
    $("#rol-id").val(null);
    $("#operation").val(1);
}

function openNewRole(){
    resetValue();
    $("#responsive-modal").modal('show');
}


