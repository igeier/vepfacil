<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Customer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;

class UploadClientFileCommand extends Command {

    protected static $defaultName = 'upload:clientfile';
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    private $container;

    public function __construct(ContainerInterface $container, LoggerInterface $logger) {
        $this->logger = $logger;
        parent::__construct();
        $this->container = $container;
    }

    protected function configure() {
        $this
            ->setDescription('Subida de archivo de clientes')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $io = new SymfonyStyle($input, $output);
            $this->logger->info('Logging like a boss at ' . __FILE__ . ':' . __LINE__);

            try {
                //$input = "././temp/plantilla_usuarios" . date("Ymd") . ".xlsx";
                $input = "/var/server/www/centraldepagos.com.ar/vepfacil/temp/plantilla_usuarios" . date("Ymd") . ".xlsx";
            } catch (\Exception $e) {
                die($output->writeln("Error en el archivo seleccionado."));
            }
            
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($input);
            $reader->setReadDataOnly(TRUE);
            $spreadsheet = $reader->load($input);
            //Asigno la hoja de calculo activa
            $spreadsheet->setActiveSheetIndex(0);
            //Obtengo el numero de filas del archivo
            $numRows = $spreadsheet->setActiveSheetIndex(0)->getHighestRow();
            
          
            $output->writeln("Procesando archivo");

            $count = 2;

            $importData = array();
            foreach ($spreadsheet->setActiveSheetIndex(0)->getRowIterator(6, $numRows) as $row) {
                $count++;
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                // echo '<tr>';

                $customerReference = '';
                $name = '';
                $address = '';
                $location = '';
                $postalCode = '';
                $province = '';
                $contactNumber = '';
                $contactNumber2 = '';
                $email = '';

                $i = -1;
                foreach ($cellIterator as $cell) {
                    $i++;
                    switch ($i) {
                        case 0:
                            $customerReference = $cell->getCalculatedValue();
                            break;
                        case 1:
                            $name = $cell->getCalculatedValue();
                            break;
                        case 2:
                            $address = $cell->getCalculatedValue();
                            break;
                        case 3:
                            $location = $cell->getCalculatedValue();
                            break;
                        case 4:
                            $postalCode = $cell->getCalculatedValue();
                            break;
                        case 5:
                            $province = $cell->getCalculatedValue();
                            break;
                        case 6:
                            $contactNumber = $cell->getCalculatedValue();
                            break;
                        case 7:
                            $contactNumber2 = $cell->getCalculatedValue();
                            break;
                        case 8:
                            $email = $cell->getCalculatedValue();
                            break;
                    }
                }
                $importData[] = array(
                    "Numero de Contrato" => $customerReference,
                    "Nombre del Contrato" => $name,
                    "Direccion" => $address,
                    "Localidad" => $location,
                    "Codigo Postal" => $postalCode,
                    "Provincia" => $province,
                    "Telefono Contacto" => $contactNumber,
                    "Telefono Contacto 2" => $contactNumber2,
                    "Email" => $email,
                );

                $em = $this->container->get('doctrine')->getEntityManager();
                $customer = $this->container->get('doctrine')->getRepository('App:Customer');
                $db = $em->getConnection();

                $com = "SELECT customer_reference FROM customer WHERE customer_reference = '$customerReference'";
                $st = $db->prepare($com);
                $st->execute();
                $arr = $st->fetchAll();
                $id = count($arr);
                                
                if ($arr == null) {
                    $output->writeln("Insertando Numero de Contrato: " . $customerReference);
                    $customer = new Customer();
                    $customer->setCustomerReference($customerReference);
                    $customer->setName($name);
                    $customer->setAddress($address);
                    $customer->setLocation($location);
                    $customer->setPostalCode($postalCode);
                    $customer->setProvince($province);
                    $customer->setContactNumber($contactNumber);
                    $customer->setContactNumber2($contactNumber2);
                    $customer->setEmail($email);


                    $em->persist($customer);
                    $em->flush();
                }
                
                for ($i=0; $i < $id; $i++) {
                    $val = $arr[$i];

                    if ($customerReference == $val['customer_reference']) {
                    $output->writeln("Actualizando Numero de Contrato: " . $customerReference);
                    $qd = $customer->createQueryBuilder('c');
                    $qd->update() 
                       ->set('c.name', ':name')
                       ->set('c.address', ':address')
                       ->set('c.location', ':location')
                       ->set('c.postalCode', ':postalCode')
                       ->set('c.province', ':province')
                       ->set('c.contactNumber', ':contactNumber')
                       ->set('c.contactNumber2', ':contactNumber2')
                       ->set('c.email', ':email')
                       ->where('c.customerReference = :customerReference') 
                       ->setParameter('name', $name) 
                       ->setParameter('address', $address)
                       ->setParameter('location', $location)
                       ->setParameter('postalCode', $postalCode)
                       ->setParameter('province', $province)
                       ->setParameter('contactNumber', $contactNumber)
                       ->setParameter('contactNumber2', $contactNumber2)
                       ->setParameter('email', $email)
                       ->setParameter('customerReference', $customerReference);
                    $update = $qd->getQuery();
                    $query = $update->execute();
                            
                    }else{
                    $output->writeln("Insertando Numero de Contrato: " . $customerReference);
                    $customer = new Customer();
                    $customer->setCustomerReference($customerReference);
                    $customer->setName($name);
                    $customer->setAddress($address);
                    $customer->setLocation($location);
                    $customer->setPostalCode($postalCode);
                    $customer->setProvince($province);
                    $customer->setContactNumber($contactNumber);
                    $customer->setContactNumber2($contactNumber2);
                    $customer->setEmail($email);


                    $em->persist($customer);
                    $em->flush();
                
                    }
                }
            }  
            rename($input, $input.".procesado");
                
                $output->writeln("Se proceso el archivo correctamente");
        } catch (\Exception $e) {
            die($output->writeln("Error al procesar archivo: " . $e->getMessage()));
        }
    }
}
