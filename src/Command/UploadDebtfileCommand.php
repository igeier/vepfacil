<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Customer;
use App\Entity\Debt;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;

class UploadDebtfileCommand extends Command {

    protected static $defaultName = 'upload:debtfile';
    
    private $container;

    public function __construct(ContainerInterface $container) {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure() {
        $this
            ->setDescription('Subida de archivo de deudas generando codigo agrupador')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $io = new SymfonyStyle($input, $output);
            
            try {
                //$input = "././temp/plantilla_deudas" . date("Ymd") . ".xlsx";
                $input = "/var/server/www/centraldepagos.com.ar/vepfacil/temp/plantilla_deudas" . date("Ymd") . ".xlsx";
            } catch (\Exception $e) {
                die($output->writeln("Error en el archivo seleccionado."));
            }
            
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($input);
            $reader->setReadDataOnly(TRUE);
            $spreadsheet = $reader->load($input);
            //Asigno la hoja de calculo activa
            $spreadsheet->setActiveSheetIndex(0);
            //Obtengo el numero de filas del archivo
            $numRows = $spreadsheet->setActiveSheetIndex(0)->getHighestRow();

            $output->writeln("Procesando archivo");

            $count = 2;

            $importData = array();
            foreach ($spreadsheet->setActiveSheetIndex(0)->getRowIterator(6, $numRows) as $row) {
                $count++;
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                // echo '<tr>';

                $customerReference = '';
                $referenceNumber = '';
                $amount = '';
                $expiration = '';
                $amount2 = '';
                $expiration2 = '';
                $branchOffice = '';
                $observations = '';
                $codePe = '';
                $createdAt = '';
                $status = '';

                $i = -1;
                foreach ($cellIterator as $cell) {
                    $i++;
                    switch ($i) {
                        case 0:
                            $customerReference = $cell->getCalculatedValue();
                            break;
                        case 1:
                            $referenceNumber = $cell->getCalculatedValue();
                            break;
                        case 2:
                            $amount = $cell->getCalculatedValue();
                            break;
                        case 3:
                            $expiration = $cell->getCalculatedValue();
                            break;
                        case 4:
                            $amount2 = $cell->getCalculatedValue();
                            break;
                        case 5:
                            $expiration2 = $cell->getCalculatedValue();
                            break;
                        case 6:
                            $branchOffice = $cell->getCalculatedValue();
                            break;
                        case 7:
                            $observations = $cell->getCalculatedValue();
                            break;
                    }
                }
                $importData[] = array(
                    "Numero de Contrato" => $customerReference,
                    "Secuencial" => $referenceNumber,
                    "Importe" => $amount,
                    "Vencimiento" => $expiration,
                    "Importe 2" => $amount2,
                    "Vencimiento 2" => $expiration2,
                    "Sucursal" => $branchOffice,
                    "Observaciones" => $observations,
                );
                
                $output->writeln("Insertando Numero de Contrato: " .$customerReference. " Secuencial Numero: " .$referenceNumber);

                $em = $this->container->get('doctrine')->getEntityManager();
                $db2 = $em->getConnection();
                    $com2 = "SELECT MAX(code_pe) as code_pe FROM debt";
                    $st2 = $db2->prepare($com2);
                    $st2->execute();
                    $arr2 = $st2->fetch();
                    $code2 = count($arr2);
                    $valor2 = $arr2['code_pe'];
                $query = $this->container->get('doctrine')->getRepository('App:Debt');
                $qd = $query->createQueryBuilder('d');
                $qd->delete()
                    ->where('d.sent = :sent')
                    ->andWhere('d.codePe < :codePe')
                    ->setParameter('sent',1)
                    ->setParameter('codePe', $valor2);
                $delete = $qd->getQuery();
                $result = $delete->getResult();
                
                $customer = $this->container->get('doctrine')->getRepository('App:Customer');
                $customerReferenceId = $customer->findOneBy(array('customerReference' => $customerReference))->getId();
                
                $db = $em->getConnection();
                $com = "SELECT d.customer_reference_id as customer_reference "
                     . "FROM debt d "
                     . "left join customer c on c.id = d.customer_reference_id "
                     . "where c.customer_reference = " .$customerReference;
                $st = $db->prepare($com);
                $st->execute();
                $arr = $st->fetch();
                //$id = count($arr);
                $ref = $arr['customer_reference'];
                    
                if ($customerReferenceId == $ref) {
                    $cpe = $query->findOneBy(array('customerReferenceId' => $customerReferenceId))->getCodePe();
                    $debt = new Debt();
                    $debt->setCustomerReferenceId($customerReferenceId);
                    $debt->setReferenceNumber($referenceNumber);
                    $debt->setAmount($amount);
                    $expiration_date = date("Y-m-d H:i:s", strtotime($expiration));
                    $debt->setExpiration(new \DateTimeImmutable($expiration_date));
                    $debt->setAmount2($amount2);
                    $expiration_date2 = date("Y-m-d H:i:s", strtotime($expiration2));
                    $debt->setExpiration2(new \DateTimeImmutable($expiration_date2));
                    $debt->setBranchOffice($branchOffice);
                    $debt->setObservations($observations);
                    //$codePe = "9900000000000".$referenceNumber;
                    $debt->setCodePe($cpe);
                    $createdAt = date("Y-m-d H:i:s");
                    $debt->setCreatedAt(new \DateTimeImmutable($createdAt));
                    $status = 1;
                    $debt->setStatusId($status);
                    $sent = 0;
                    $debt->setSent($sent);
                    
                    $em->persist($debt);
                    $em->flush();
                
                }else {
                    $debt = new Debt();
                    $debt->setCustomerReferenceId($customerReferenceId);
                    $debt->setReferenceNumber($referenceNumber);
                    $debt->setAmount($amount);
                    $expiration_date = date("Y-m-d H:i:s", strtotime($expiration));
                    $debt->setExpiration(new \DateTimeImmutable($expiration_date));
                    $debt->setAmount2($amount2);
                    $expiration_date2 = date("Y-m-d H:i:s", strtotime($expiration2));
                    $debt->setExpiration2(new \DateTimeImmutable($expiration_date2));
                    $debt->setBranchOffice($branchOffice);
                    $debt->setObservations($observations);
                    $db1 = $em->getConnection();
                    $com1 = "SELECT MAX(code_pe) as code_pe FROM debt";
                    $st1 = $db1->prepare($com1);
                    $st1->execute();
                    $arr1 = $st1->fetch();
                    $code = count($arr1);
                    $valor = $arr1['code_pe'];
                    $val = substr($valor, 2, 17);
                    $val2 = $val + 1;
                    $format4 = str_pad(strval($val2), "17", "0", STR_PAD_LEFT);
                    $codePe = "99".$format4;
                    $debt->setCodePe($codePe);
                    $createdAt = date("Y-m-d H:i:s");
                    $debt->setCreatedAt(new \DateTimeImmutable($createdAt));
                    $status = 1;
                    $debt->setStatusId($status);
                    $sent = 0;
                    $debt->setSent($sent);
                
                    $em->persist($debt);
                    $em->flush();
                
                }
                
                }
        
            rename($input, $input.".procesado");
            
            $output->writeln("Se proceso el archivo correctamente");
        } catch (\Exception $e) {
            die($output->writeln("Error al procesar archivo: " . $e->getMessage()));
        }
    }
}
