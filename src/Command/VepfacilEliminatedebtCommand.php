<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Customer;
use App\Entity\Debt;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;

class VepfacilEliminatedebtCommand extends Command
{
    protected static $defaultName = 'vepfacil:eliminatedebt';
    
    private $container;

    public function __construct(ContainerInterface $container) {
        parent::__construct();
        $this->container = $container;
    }
    
    protected function configure()
    {
        $this
            ->setDescription('Elimina deudas del conector que luego seran enviadas al SIMP')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        $em = $this->container->get('doctrine')->getManager();
        $db = $em->getConnection();

        $query = "SELECT c.customer_reference, d.reference_number, d.amount, d.expiration, "
                . "d.code_pe "
                . "FROM `debt` d "
                . "LEFT JOIN customer c on c.id = d.customer_reference_id "
                . "WHERE d.sent = 2 "
                . "LIMIT 250";
        $stmt = $db->prepare($query);
        $params = array();
        $stmt->execute($params);
        $row = $stmt->fetchAll();
        /*$em = $this->container->get('doctrine')->getManager();
        $debt = $em->getRepository("App:Debt");
        $row = $debt->createQueryBuilder('d')
            //->leftJoin('d.customerReferenceId', 'c')
            ->where('d.sent = :sent')
            //->groupBy('d.codePe')
            ->setParameter('sent', 0)
            ->setMaxResults(250)
            ->getQuery()
            ->getArrayResult();*/
        
        $temp = array();
        foreach ($row as $item) {
            $current = array();
            $key = $item['code_pe']; 
            $date = date("Ymd", strtotime($item['expiration']));
            //$date = $expiration->format('Ymd');
            $customerReference = $item['customer_reference'];
            $referenceNumber = intval($item['reference_number']);
            $amount = intval($item['amount']);
            $temp[strval($key)][] = array(
                    'vencimientoFactura' => $date,
                    'numeroReferencia' => $customerReference,
                    'importeFactura' => $amount,
                    'tieneComplementos' => false,
                    'numeroSecuencialFactura' => $referenceNumber,
                    'identificacionDeudor' => $customerReference
            );
        }
        $toSend["cpeArray"] = array();
        foreach ($temp as $key => $value) {
            $current = array();
            $current["codigoPagoElectronico"] = $key;
            $current["vencimientoPago"] = $date;
            $current["facturas"] = array();
            $current["facturas"] = $temp[$key];
            $toSend["cpeArray"][]= $current;
        }
        
        $output->writeln(json_encode($toSend));
        
            $client = new \GuzzleHttp\Client();

                $response = $client->request('POST', 'http://schubert.microblet.com/metrogas-cdp/api/deudas/baja', [

                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer t0HBe6KnwoBV4Uflfb8mi1cgeFPRq4HX'
                    ],
                    
                    'body' => json_encode($toSend)
                    /*'form_params' => [
                    'codigoPagoElectronico' => $key
                    ]*/

                ]);

                //$body = $response->getBody();
        
        /*$ejemploRespuesta = array(
            "failed" => ['9900000000000005200', '9900000000000005300', '9900000000000006001', '9900000000000006102']
        );*/
        
        foreach ($row as $act) {
            /*$b = array();
            $b["failed"] = array();*/
          $codePe = $act['code_pe'];
        
        $entityManager = $this->container->get('doctrine')->getRepository('App:Debt');
        $debt = $entityManager->findOneBy(array('codePe' => $codePe))->getCodePe();
        //$output->writeln($codePe);
        $qd = $entityManager->createQueryBuilder('d');
        $qd->update() 
           ->set('d.sent', ':sent')
           ->where('d.codePe = :codePe') 
           ->setParameter('sent', 1)
           ->setParameter('codePe', $debt);
        $update = $qd->getQuery();
        $query = $update->execute();

        }
    }
}
