<?php

namespace App\Controller;

use \PDO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Internal\Hydration;
use Doctrine\ORM\Query;
use App\Entity\Debt;
use GuzzleHttp\Client;

class AjaxController extends AbstractController
{
    /**
     * @Route("/ajax/sample", name="ajax-sample")
     */
    public function ajaxSample(Request $request)
    {
        /* Proceso datos */
        return new JsonResponse(array("success" => true, "data" => $request->get("variable")));
    }
    
    /**
     * @Route("/editvep", name="editvep")
     */
    public function editvep()
    {
        # conectare la base de datos
        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();
        
	/*Inicia validacion del lado del servidor*/
	if (empty($_POST['id'])) {
           $errors[] = "ID vacío";
        } else if (empty($_POST['customer_reference'])){
			$errors[] = "Numero de contrato vacío";
		} else if (empty($_POST['reference_number'])){
			$errors[] = "Secuencial vacío";
		} else if (empty($_POST['amount'])){
			$errors[] = "Importe vacío";
		} else if (empty($_POST['expiration'])){
			$errors[] = "Fecha de Vencimiento vacío";
		} else if (empty($_POST['observations'])){
			$errors[] = "Observaciones vacío";
		} else if (empty($_POST['code_pe'])){
			$errors[] = "CPE vacío";
		} else if (
			!empty($_POST['id']) &&
			!empty($_POST['customer_reference']) && 
			!empty($_POST['reference_number']) &&
			!empty($_POST['amount']) &&
			!empty($_POST['expiration']) &&
                        !empty($_POST['observations']) &&
			!empty($_POST['code_pe'])
			
		){
                
                $sent = 0;
                $customerReference = $_POST["customer_reference"];
		$referenceNumber = $_POST["reference_number"];
		$amount = $_POST["amount"];
		$expiration = $_POST["expiration"];
                $expiration_date = date("Y-m-d H:i:s", strtotime($expiration));
		$observations = $_POST["observations"];	
                $codePe = $_POST["code_pe"];	
                $id = intval($_POST['id']);
                
		$sql = "UPDATE debt SET amount = '".$amount."', expiration = '".$expiration_date."',
		observations = '".$observations."', sent = '".$sent."' WHERE id = '".$id."'";
		$stmt = $db->prepare($sql);
                $query_update = $stmt->execute();
                
			if ($query_update){
				$messages[] = "Los datos han sido actualizados satisfactoriamente.";
			} else{
				$errors[]= "Lo siento algo ha salido mal intenta nuevamente.";
			}
		} else {
			$errors[]= "Error desconocido.";
		}
                
                return $this->render('response.html.twig',["msg" => $messages]);
    }
    
    /**
     * @Route("/groupvep", name="groupvep")
     */
    public function groupvep()
    {
        # conectare la base de datos
        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();
        
	/*Inicia validacion del lado del servidor*/
	if (empty($_POST['id'])) {
           $errors[] = "ID vacío";
        } else if (empty($_POST['customer_reference'])){
			$errors[] = "Numero de contrato vacío";
		} else if (empty($_POST['reference_number'])){
			$errors[] = "Secuencial vacío";
		} else if (empty($_POST['amount'])){
			$errors[] = "Importe vacío";
		} else if (empty($_POST['expiration'])){
			$errors[] = "Fecha de Vencimiento vacío";
		} else if (empty($_POST['observations'])){
			$errors[] = "Observaciones vacío";
		} else if (empty($_POST['code_pe'])){
			$errors[] = "CPE vacío";
		} else if (
			!empty($_POST['id']) &&
			!empty($_POST['customer_reference']) && 
			!empty($_POST['reference_number']) &&
			!empty($_POST['amount']) &&
			!empty($_POST['expiration']) &&
                        !empty($_POST['observations']) &&
			!empty($_POST['code_pe'])
			
		){
                
                $sent = 0;
                $customerReference = $_POST["customer_reference"];
		$referenceNumber = $_POST["reference_number"];
		$amount = $_POST["amount"];
		$expiration = $_POST["expiration"];
                $expiration_date = date("Y-m-d H:i:s", strtotime($expiration));
		$observations = $_POST["observations"];
                $code_pe = $_POST["code_pe"];
                $id = $_POST['id'];
                $status_id = 3;
                
                $client = new \GuzzleHttp\Client();
                
                $response = $client->request('POST', 'https://metrogas.centraldepagos.com.ar/api/deudas/baja', [

                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer t0HBe6KnwoBV4Uflfb8mi1cgeFPRq4HX'
                    ],
                    
                    //'body' => json_encode($toSend)
                    'form_params' => [
                    'codigoPagoElectronico' => $code_pe
                    ]

                ]);

                //$body = $response->getBody();
                          
                $db1 = $em->getConnection();
                $com1 = "SELECT MAX(code_pe) as code_pe FROM debt";
                $st1 = $db1->prepare($com1);
                $st1->execute();
                $arr1 = $st1->fetch();
                $code = count($arr1);
                $valor = $arr1['code_pe'];
                $val = substr($valor, 2, 17);
                $val2 = $val + 1;
                $format4 = str_pad(strval($val2), "17", "0", STR_PAD_LEFT);
                $codePe = "99".$format4;
                
		$sql = "UPDATE debt SET expiration = '".$expiration_date."', expiration2 = '".$expiration_date."', "
                     . "observations = '".$observations."', code_pe = '".$codePe."', sent = '".$sent."' "
                     . "WHERE id in ($id)";
		$stmt = $db->prepare($sql);
                $query_update = $stmt->execute();
                
                if ($query_update){
                        $messages[] = "Los datos han sido actualizados satisfactoriamente.";
                } else{
                        $errors[]= "Lo siento algo ha salido mal intenta nuevamente.";
                }
		} else {
			$errors[]= "Error desconocido.";
		}
                                
                $sql2 = "UPDATE debt SET status_id = '".$status_id."' "
                      . "WHERE code_pe = '".$code_pe."' "
                      . "AND id not in ($id)";
		$stmt2 = $db->prepare($sql2);
                $query_update2 = $stmt2->execute();
             
		return $this->render('response.html.twig',["msg" => $messages]);
    }
}
