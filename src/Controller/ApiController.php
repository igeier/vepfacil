<?php

namespace App\Controller;

use \PDO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Internal\Hydration;
use Doctrine\ORM\Query;
use App\Entity\Debt;

class ApiController extends AbstractController
{
    /**
     * Lista todos los clientes de ejemplo.
     * 
     * Esta operación devuelve un vector con una lista de clientes de ejemplo.
     * 
     * @Route("/api/sample-clients", name="api-sample", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Vector de clientes"
     * )
     * @SWG\Tag(name="rewards")
     */
    public function sampleClients()
    {
        /* 
            Acá y en toda operación de la API (o en el constructor, o en un listener más global) debería
            chequearse que la cabecera de Authorization venga con un token, y que sea válido
        */
        $arr = array();
        $arr[] = array(
            "nombre" => "Nicolas",
            "apellido" => "Bullorini"
        );
        $arr[] = array(
            "nombre" => "Juan",
            "apellido" => "Perez"
        );
        return new JsonResponse($arr);
    }
    
    /** 
     * @Route("/api/payment", name="api-payment", methods={"POST"})
     * 
     * @SWG\Response(
     *     response=200,
     *     description="The board was edited successfully."
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="An error has occurred trying to edit the board."
     * 
     * )
     * @SWG\Tag(name="debt")
     */
    public function editDebtAction(Request $request)
    {
        //$serializer = $this->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $debt = [];       
        $message = "";
 
        try {
            $code = 201;
            $error = false;
            $codePe = $request->get('code_pe');
    
            if (!is_null($codePe)) {
                $entityManager = $this->container->get('doctrine')->getRepository('App:Debt');
                $debt = $entityManager->findOneBy(array('codePe' => $codePe))->getCodePe();
                $qd = $entityManager->createQueryBuilder('d');
                $qd->update() 
                   ->set('d.statusId', ':status_id')
                   ->where('d.codePe = :codePe') 
                   ->setParameter('status_id', 2)
                   ->setParameter('codePe', $debt);
                $update = $qd->getQuery();
                $query = $update->execute();
      
            } else {
                $code = 500;
                $error = true;
                $message = "Se ha producido un error al intentar actualizar la deuda. Error: debe proporcionar todos los campos obligatorios";
            }
            
        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "Se ha producido un error intentando añadir una nueva deuda. - Error: {$ex->getMessage()}";
        }
        
        $response = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 201 ? $debt : $message,
        ];
        
        return new JsonResponse($response);
    }
}
