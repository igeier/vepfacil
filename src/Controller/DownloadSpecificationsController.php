<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Fpdf\Fpdf;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


class DownloadSpecificationsController extends Controller {

    /**
     * @Route("/ficha-tecnica", name="app_download_specifications")
     */
    public function downloadSpecifications() {
        $html = $this->renderView('pdf.html.twig');

        $filename = sprintf('specifications-%s.pdf', date('Y-m-d-hh-ss'));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
            ]
        );
    }
}
?>