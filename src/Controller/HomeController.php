<?php

namespace App\Controller;

use App\Entity\Debt;
use App\Entity\Debtxclient;
use App\Entity\User;
use Doctrine\ORM\Query;
use GuzzleHttp\Client;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Swift_Mailer;
use Swift_SmtpTransport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route("/base", name="base")
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }

    /**
     * @Route("/", name="home")
     */
    public function paginaprincipal()
    {
        return $this->render('users.html.twig');
    }

    /**
     * @Route("/customer", name="cusLogin")
     */
    public function cusLogin()
    {
        return $this->render('customer_login.html.twig');
    }

    /**
     * @Route("/NewUser", name="NewUser")
     */
    public function newUser()
    {
        if (isset($_POST['NewUser'])) {
            if (empty($_POST['username'])) {
                $errors[] = 'Nombre de usuario vacío';
            } elseif (empty($_POST['email'])) {
                $errors[] = 'Email vacío';
            } elseif (empty($_POST['password'])) {
                $errors[] = 'Contraseña vacía';
            } elseif (empty($_POST['role'])) {
                $errors[] = 'Rol de usuario vacío';
            } elseif (
                !empty($_POST['username']) &&
                !empty($_POST['email']) &&
                !empty($_POST['password']) &&
                !empty($_POST['role'])
            ) {
                $new_username = $_POST['username'];
                $new_email = $_POST['email'];
                $new_pass = $_POST['password'];
                $new_role = $_POST['role'];

                $userManager = $this->get('fos_user.user_manager');
                $email_exist = $userManager->findUserByEmail($new_email);

                if ($email_exist) {

                    $this->addFlash('error', 'Usuario ya registrado con el mail utilizado.');
                    return $this->render('register.html.twig');
                }
                $em = $this->getDoctrine()->getManager();
                $user = new User();
                $user->setUsername($new_username);
                $user->setUsernameCanonical(strtolower($new_username));
                $user->setEmail($new_email);
                $user->setEmailCanonical(strtolower($new_email));
                $user->setEnabled(1);
                $user->setPlainPassword($new_pass);

                $user->setSalt(null);

                $em->persist($user);
                $em->flush();

                if ($new_role = 'ROLE_ADMIN') {
                    $user_role = $userManager->findUserByEmail($new_email);
                    $user_role->addRole($new_role);
                    $userManager->updateUser($user_role);
                }

                $this->addFlash('success', 'Usuario registrado con éxito.');
                return $this->render('register.html.twig');
            }
        }
        return $this->render('register.html.twig');
    }


    /**
     * @Route("/debts", name="debts")
     */
    public function debts()
    {
        $em = $this->getDoctrine()->getManager();
        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = [
            'url' => 'mysql://root:root@10.150.0.5:3306/simp',
        ];

        $db = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        $query = 'SELECT db.batch_date, db.batch_number, dm.description as motive, d.reference_number, c.name, '
               . 'ds.description, d.amount1, d.expiration_date1 '
               . 'FROM simp.debt d '
               . 'left join client c on c.client_id = d.client_id '
               . 'left join debt_batch db on db.debt_batch_id = d.debt_batch_id '
               . 'left join debt_motive dm on dm.debt_motive_id = d.debt_motive_id '
               . 'left join debt_status ds on ds.status_id = d.status_id '
               . 'where c.company_id = 246 order by 1 desc';
        $stmt = $db->prepare($query);
        $params = [];
        $stmt->execute($params);
        $arr = $stmt->fetchAll();

        $nr = count($arr);
        //echo $nr;
        //print $arr;
        $arrCol = ['', 'Fecha del lote', 'Numero de lote', 'Motivo', 'Numero de Referencia', 'Nombre', 'Estado de deuda',
            'Importe1', 'Vencimiento1',];
        $nc = count($arrCol);

        return $this->render('pp.html.twig', ['arr' => $arr, 'arrCol' => $arrCol]);
    }

    /**
     * @Route("/UserAdmin", name="useradmin")
     */
    public function userAdminList()
    {

        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();

        $query = "SELECT id, username, email, enabled, company FROM fos_user";
        $stmt = $db->prepare($query);
        $params = [];
        $stmt->execute($params);
        $arr = $stmt->fetchAll();
        $arrCol = ['Id', 'Nombre de usuario', 'Email', 'Estado', 'Empresa', 'Acciones'];

        return $this->render('useradmin.html.twig', ['arr' => $arr, 'arrCol' => $arrCol]);
    }

    /**
     * @Route("/misveps", name="misveps")
     */
    public function misveps(Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        $db = $em->getConnection();

        $query = 'SELECT d.id, c.customer_reference, d.reference_number, d.amount, d.expiration, d.branch_office, '
               . 'd.observations, d.code_pe, s.name '
               . 'FROM `debt` d '
               . 'LEFT JOIN customer c on c.id = d.customer_reference_id '
               . 'LEFT JOIN status s on s.id = d.status_id '
               . 'WHERE s.id = 1 ';
        $stmt = $db->prepare($query);
        $params = [];
        $stmt->execute($params);
        $arr = $stmt->fetchAll();

        $nr = count($arr);
        //echo $nr;
        //print $arr;
        $arrCol = ['', '', 'Número de Contrato', 'Secuencial', 'Monto', 'Vencimiento', 'Observaciones', 'CPE', 'Estado'];
        $nc = count($arrCol);

        $query2 = 'SELECT c.customer_reference, ANY_VALUE(d.reference_number) AS reference_number, '
                . 'ROUND(SUM(d.amount),2) AS amount, MAX(d.expiration) AS expiration, '
                . 'ANY_VALUE(d.branch_office) AS branch_office, ANY_VALUE(d.observations) AS observations, '
                . 'd.code_pe, ANY_VALUE(s.name) AS name '
                . 'FROM vep_mg.debt d '
                . 'LEFT JOIN vep_mg.customer c on c.id = d.customer_reference_id '
                . 'LEFT JOIN vep_mg.status s on s.id = d.status_id '
                . 'WHERE s.id = 2 '
                . 'GROUP BY c.customer_reference, d.code_pe';
        $stmt2 = $db->prepare($query2);
        $params2 = [];
        $stmt2->execute($params2);
        $arr2 = $stmt2->fetchAll();

        $nr2 = count($arr2);
        //echo $nr;
        //print $arr;
        $arrCol2 = ['', 'Número de Contrato', 'Secuencial', 'Monto', 'Vencimiento', 'Observaciones', 'CPE', 'Estado'];
        $nc2 = count($arrCol2);

        if (isset($_POST['Seleccion'])) {
            $var = $_POST['Seleccion'];

            if ('1' == $var) {
                $message = (new \Swift_Message('Notificacion Metrogas 1 '))
                    ->setFrom('ab982042@gmail.com')
                    ->setTo(filter_input(INPUT_POST, 'mailMisV'))
                    ->setBody('Holaaa')
                    ->addPart('<hr size="2" /><br/><label><ul>@Referencia del Cliente – Gracias por haber generado '
                        . 'el CODIGO ELECTRONICO DE PAGO XXXXXXXXXXXXXXXX, (referencia CPE) por un monto total '
                        . 'de $ Importe Usted podrá cancelar el volante de pago electrónico (VEP) hasta la '
                        . 'fecha de vencimiento que se visualiza en el comprobante.</ul><br/><br/> <pre> '
                        . 'Presente  el siguiente CODIGO ELECTRONICO DE PAGO (referencia CPE) en cualquiera '
                        . 'de las bocas de cobranza de Pago Fácil, en Rapipago o en Cobro Express. Recuerde '
                        . 'que podra hacerlo hasta la fecha de vencimiento impreso en el volante electrónico '
                        . 'de pago (VEP). Gracias por usar los canales de servicios de cobranza alternativos '
                        . 'de MetroGAS. </pre><ul><ul><ul><h1>XXXXXXXXXXXXX XXXXXXXXXXXXX XXXXXXXXXXXXX</h1>'
                        . '</ul></ul></ul><br><br/><ul><hr size="2" /><font size="1">Le recordamos que Ud '
                        . 'generó el  CODIGO ELECTRONICO DE PAGO (referencia CPE) XXXXXXXXXXXXXXXX por un '
                        . 'monto total de $ Importe y cuya fecha de vencimiento es el FECHA DE VENCIMIENTO. '
                        . 'Podrá cancelar la deuda  en cualquiera de las bocas de cobranza de Pago Fácil, '
                        . 'en Rapipago o en Cobro Express con el número del CEP generado en el cupón de pago. '
                        . 'Si Usted, ya pagó la deuda, la misma se verá reflejada en su cuenta a las 72 horas '
                        . 'hábiles de haberla cancelado. Gracias por usar los canales de servicios de cobranza '
                        . 'alternativos de MetroGAS.</font></ul>', 'text/html');
            } else {
                $message = (new \Swift_Message('Notificacion Metrogas 2 '))
                    ->setFrom('ab982042@gmail.com')
                    ->setTo(filter_input(INPUT_POST, 'mailMisV'))
                    ->setBody('Chaau')
                    ->addPart(
                        '<hr size="2" /><br/><label><ul>@Referencia del Cliente – Gracias por haber generado el '
                        . 'CODIGO ELECTRONICO DE PAGO XXXXXXXXXXXXXXXX, (referencia CPE) por un monto total de $ '
                        . 'Importe Usted podrá  cancelar el  volante de pago electrónico  (VEP) hasta la fecha de '
                        . 'vencimiento que se visualiza en el comprobante.</ul><br/><br/> <pre> Presente el '
                        . 'siguiente CODIGO ELECTRONICO DE PAGO (referencia CEP) en cualquiera de las bocas de '
                        . 'cobranza de Pago Fácil, en Rapipago o en Cobro Express. Recuerde que podra hacerlo '
                        . 'hasta la fecha de vencimiento impreso en el volante electrónico de pago (VEP). '
                        . 'Gracias por usar los canales de servicios de cobranza alternativos de MetroGAS. '
                        . '</pre><ul><ul><ul><h1>XXXXXXXXXXXXX XXXXXXXXXXXXX XXXXXXXXXXXXX</h1></ul></ul></ul><br>'
                        . '<br/><ul><hr size="2" /><font size="1">Le recordamos que Ud generó el CODIGO '
                        . 'ELECTRONICO DE PAGO (referencia CPE) XXXXXXXXXXXXXXXX por un monto total de $ Importe '
                        . 'y cuya fecha de vencimiento es el FECHA DE VENCIMIENTO. Podrá cancelar la deuda en '
                        . 'cualquiera de las bocas de cobranza de Pago Fácil, en Rapipago o en Cobro Express con '
                        . 'el número del CEP generado en el cupón de pago. Si Usted, ya pagó la deuda, la misma '
                        . 'se verá reflejada en su cuenta a las 72 horas hábiles de haberla cancelado. Gracias '
                        . 'por usar los canales de servicios de cobranza alternativos de MetroGAS.</font></ul>',
                        'text/html'
                    );
            }
            /*
             * If you also want to include a plaintext version of the message
            ->addPart( EL ERROR DE SSL DE CERTIFICADOS ES UN ERROR POR EL ANTIVIRUS!!!!!
                    $this->renderView(
                            'emails/registration.txt.twig',
                            ['name' => $name]
                    ),
                    'text/plain'
            )
            */
            $transporte = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'));

            $transporte->setUsername('ab982042@gmail.com');
            $transporte->setPassword('buenokaliesunbeef4_AsD');

            $mailer = (new Swift_Mailer($transporte));

            $mailer->send($message);
        }

        return $this->render('misveps.html.twig', ['arr' => $arr, 'arrCol' => $arrCol, 'arr2' => $arr2, 'arrCol2' => $arrCol2]);
    }

    /**
     * @Route("/nuevovep", name="nuevovep")
     */
    public function nuevovep(Swift_Mailer $mailer)
    {
        if (isset($_POST['nuevovep'])) {
            // Inicia validacion del lado del servidor
            if (empty($_POST['busqueda'])) {
                $errors[] = 'Numero de contrato vacío';
            } elseif (empty($_POST['referenceNumber'])) {
                $errors[] = 'Secuencial vacío';
            } elseif (empty($_POST['amount'])) {
                $errors[] = 'Importe vacío';
            } elseif (empty($_POST['expiration'])) {
                $errors[] = 'Fecha de vencimiento vacío';
            } elseif (
                !empty($_POST['busqueda']) &&
                !empty($_POST['referenceNumber']) &&
                !empty($_POST['amount']) &&
                !empty($_POST['expiration'])
            ) {
                $customer_reference = $_POST['busqueda'];
                $reference_number = $_POST['referenceNumber'];
                $amount = $_POST['amount'];
                $expiration = $_POST['expiration'];
                $observations = $_POST['observations'];

                $em = $this->getDoctrine()->getManager();
                $customer = $this->getDoctrine()->getRepository('App:Customer');
                $customerReferenceId = $customer->findOneBy(['customerReference' => $customer_reference])->getId();
                /*$query = $this->getDoctrine()->getRepository('App:Debt');

                $db = $em->getConnection();
                $com = 'SELECT d.customer_reference_id as customer_reference '
                    . 'FROM debt d '
                    . 'left join customer c on c.id = d.customer_reference_id '
                    . 'where c.customer_reference = ' . $customer_reference;
                $st = $db->prepare($com);
                $st->execute();
                $arr = $st->fetch();
                //$id = count($arr);
                $ref = $arr['customer_reference'];

                if ($customerReferenceId == $ref) {
                    $cpe = $query->findOneBy(['customerReferenceId' => $customerReferenceId])->getCodePe();
                    $debt = new Debt();
                    $debt->setCustomerReferenceId($customerReferenceId);
                    $debt->setReferenceNumber($reference_number);
                    $debt->setAmount($amount);
                    $expiration_date = date('Y-m-d H:i:s', strtotime($expiration));
                    $debt->setExpiration(new \DateTimeImmutable($expiration_date));
                    $debt->setAmount2($amount);
                    $expiration_date2 = date('Y-m-d H:i:s', strtotime($expiration));
                    $debt->setExpiration2(new \DateTimeImmutable($expiration_date2));
                    $branchOffice = 0;
                    $debt->setBranchOffice($branchOffice);
                    $debt->setObservations($observations);
                    //$codePe = "9900000000000".$referenceNumber;
                    $debt->setCodePe($cpe);
                    $createdAt = date('Y-m-d H:i:s');
                    $debt->setCreatedAt(new \DateTimeImmutable($createdAt));
                    $status = 1;
                    $debt->setStatusId($status);
                    $sent = 0;
                    $debt->setSent($sent);

                    $em->persist($debt);
                    $em->flush();

                    if ($debt) {
                        $messages[] = 'Los datos han sido guardados satisfactoriamente.';
                    } else {
                        $errors[] = 'Lo siento algo ha salido mal intenta nuevamente.';
                    }
                } else {*/
                $debt = new Debt();
                $debt->setCustomerReferenceId($customerReferenceId);
                $debt->setReferenceNumber($reference_number);
                $debt->setAmount($amount);
                $expiration_date = date('Y-m-d H:i:s', strtotime($expiration));
                $debt->setExpiration(new \DateTimeImmutable($expiration_date));
                $debt->setAmount2($amount);
                $expiration_date2 = date('Y-m-d H:i:s', strtotime($expiration));
                $debt->setExpiration2(new \DateTimeImmutable($expiration_date2));
                $branchOffice = 0;
                $debt->setBranchOffice($branchOffice);
                $debt->setObservations($observations);
                $db1 = $em->getConnection();
                $com1 = 'SELECT MAX(code_pe) as code_pe FROM debt';
                $st1 = $db1->prepare($com1);
                $st1->execute();
                $arr1 = $st1->fetch();
                $code = count($arr1);
                $valor = $arr1['code_pe'];
                $val = substr($valor, 2, 17);
                $val1 = $val + 1;
                $format = str_pad(strval($val1), '17', '0', STR_PAD_LEFT);
                $codePe = '99' . $format;
                $debt->setCodePe($codePe);
                $createdAt = date('Y-m-d H:i:s');
                $debt->setCreatedAt(new \DateTimeImmutable($createdAt));
                $status = 1;
                $debt->setStatusId($status);
                $sent = 0;
                $debt->setSent($sent);

                $em->persist($debt);
                $em->flush();
                //}
                /*
                if ($debt) {
                    $messages[] = 'Los datos han sido guardados satisfactoriamente.';
                    $mail = (new \Swift_Message('Contacto Nuevo Vep'))
                        ->setFrom('ab982042@gmail.com')
                        ->setTo(filter_input(INPUT_POST, 'mail'))
                        ->setBody('MAIL DE PRUEBA')
                        ->addPart('<hr size="2" /><br/><label><ul>'.filter_input(INPUT_POST, 'IdRef').' '
                                .'– Gracias por haber generado el CODIGO ELECTRONICO DE PAGO '
                                .''."{$codePe}".', (referencia CPE) por un monto total de '
                                .'$'."{$amount}".filter_input(INPUT_POST, 'Imp').' Usted podrá cancelar el volante de '
                                .'pago electrónico (VEP) hasta la fecha de vencimiento que se visualiza en '
                                .'el comprobante.</ul><br/><br/> <pre>Presente el siguiente CODIGO '
                                .'ELECTRONICO DE PAGO (referencia CPE) en cualquiera de las bocas de '
                                .'cobranza de Pago Fácil, en Rapipago o en Cobro Express. Recuerde que '
                                .'podra hacerlo hasta la fecha de vencimiento impreso en el volante '
                                .'electrónico de pago (VEP). Gracias por usar los canales de servicios de '
                                .'cobranza alternativos de MetroGAS. </pre><ul><ul><ul><h1>'
                                .''."{$codePe}".'</h1></ul></ul></ul><br><br/>'
                                .'<ul><hr size="2" /><font size="1">Le recordamos que Ud generó el CODIGO '
                                .'ELECTRONICO DE PAGO (referencia CPE) '."{$codePe}".' por un monto total '
                                .'de $'."{$amount}".filter_input(INPUT_POST, 'Imp').' y cuya fecha de vencimiento es '
                                .'el '."{$expiration}".filter_input(INPUT_POST, 'fec').'. Podrá cancelar la deuda en '
                                .'cualquiera de las bocas de cobranza de Pago Fácil, en Rapipago o en '
                                .'Cobro Express con el número del CPE generado en el cupón de pago. '
                                .'Si Usted, ya pagó la deuda, la misma se verá reflejada en su cuenta a '
                                .'las 72 horas hábiles de haberla cancelado. Gracias por usar los canales '
                                .'de servicios de cobranza alternativos de MetroGAS.'
                                .'</font></ul>', 'text/html')
                    ;
                    $transporte = (new Swift_SmtpTransport("smtp.gmail.com", 465, "ssl"));


                  $transporte->setUsername("ab982042@gmail.com");
                  $transporte->setPassword("buenokaliesunbeef4_AsD");


                   $mailer = (new Swift_Mailer($transporte));

                    $mailer->send($mail);
                } else {
                    $errors[] = 'Lo siento algo ha salido mal intenta nuevamente.';
                }
            } else {
                $errors[] = 'Error desconocido.';
            }

            if (isset($errors)) {
                foreach ($errors as $error) {
                    echo $error;
                }
            }
            if (isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }*/
                if ($debt) {
                    $messages[] = 'Los datos han sido guardados satisfactoriamente.';
                } else {
                    $errors[] = 'Lo siento algo ha salido mal intenta nuevamente.';
                }
            } else {
                $errors[] = 'Error desconocido.';
            }

            if (isset($errors)) {
                foreach ($errors as $error) {
                    echo $error;
                }
            }
            if (isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }
            }
        }

        return $this->render('new_vep.html.twig');
    }

    /**
     * @Route("/grid", name="grid")
     */
    public function grid()
    {
        //Variable vacía (para evitar los E_NOTICE)
        $mensaje = '';

        if (isset($_POST['valorBusqueda'])) {
            //Variable de búsqueda
            $consultaBusqueda = $_POST['valorBusqueda'];
            //Selecciona todo de la tabla debt
            //donde el customer_reference sea igual a $consultaBusqueda
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $query2 = 'SELECT d.id, c.customer_reference, d.reference_number, d.amount, d.expiration,d.branch_office, '
                    . 'd.observations, d.code_pe '
                    . 'FROM `debt` d '
                    . 'LEFT JOIN customer c on c.id = d.customer_reference_id '
                    . 'WHERE c.customer_reference = ' . $consultaBusqueda. ' AND status_id not in (2,3)';
            $stmt2 = $db->prepare($query2);
            $params2 = [];
            $stmt2->execute($params2);
            $arr2 = $stmt2->fetchAll();

            $nr2 = count($arr2);

            $arrCol2 = ['Nro', 'Número de Contrato', 'Secuencial', 'Monto', 'Vencimiento', 'Observaciones', 'CPE', 'Editar'];
            $nc2 = count($arrCol2);

            //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
            if (0 === $nr2) {
                $mensaje = '<p>No se encontro resultados para <strong>' . $consultaBusqueda . '</strong></p>';
            } else {
                //Si existe alguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
                echo 'Resultados para <strong>' . $consultaBusqueda . '</strong>';
            } //Fin else $filas
        }//Fin isset $consultaBusqueda

        //Devolvemos el mensaje que tomará jQuery
        echo $mensaje;

        return $this->render('grid.html.twig', ['arr2' => $arr2, 'arrCol2' => $arrCol2]);
    }

    /**
     * @Route("/upload-client", name="upload-client")
     */
    public function generateClient(Request $request)
    {
        $format = ['xls', 'xlsx'];
        if ('POST' == $request->getMethod()) {
            $file = $request->files->get('file-xls');
            $archivo = $_FILES['file-xls']['name'];
            $extension = pathinfo($archivo, PATHINFO_EXTENSION);
            if (!$file) {
                $this->addFlash('error', 'No hay archivos seleccionados.');

                return $this->render('upload_debt.html.twig');
            }
            if (!in_array($extension, $format)) {
                $this->addFlash('error', 'Error formato no permitido.');

                return $this->render('upload_debt.html.twig');
            }

            try {
                $mid = 'plantilla_usuarios' . date('Ymd');
                $dir = $this->get('kernel')->getRootDir() . '/../temp';

                try {
                    $file->move($dir, $mid . '.xlsx');
                } catch (\Exception $e) {
                    $this->addFlash('error', 'Error en el archivo seleccionado.');

                    return $this->render('upload_client.html.twig');
                }

                $this->addFlash('success', 'Archivo subido correctamente.');

                return $this->render('upload_client.html.twig');
            } catch (\Exception $e) {
                $this->addFlash('error', 'Error al subir archivo: ') . $e->getMessage();
            }
        }

        return $this->render('upload_client.html.twig');
    }

    /**
     * @Route("/upload-debt", name="upload-debt")
     */
    public function generateDebt(Request $request)
    {
        $format = ['xls', 'xlsx'];
        if ('POST' == $request->getMethod()) {
            $file = $request->files->get('file-xls');
            $archivo = $_FILES['file-xls']['name'];
            $extension = pathinfo($archivo, PATHINFO_EXTENSION);
            if (!$file) {
                $this->addFlash('error', 'No hay archivos seleccionados.');

                return $this->render('upload_debt.html.twig');
            }
            if (!in_array($extension, $format)) {
                $this->addFlash('error', 'Error formato no permitido.');

                return $this->render('upload_debt.html.twig');
            }

            try {
                $mid = 'plantilla_deudas' . date('Ymd');
                $dir = $this->get('kernel')->getRootDir() . '/../temp';

                try {
                    $file->move($dir, $mid . '.xlsx');
                } catch (\Exception $e) {
                    $this->addFlash('error', 'Error en el archivo seleccionado.');

                    return $this->render('upload_debt.html.twig');
                }

                $this->addFlash('success', 'Archivo subido correctamente');

                return $this->render('upload_debt.html.twig');
            } catch (\Exception $e) {
                $this->addFlash('error', 'Error al subir archivo: ') . $e->getMessage();

                return $this->render('upload_debt.html.twig');
            }
        }

        return $this->render('upload_debt.html.twig');
    }

    /**
     * @Route("/upload-debtxclient", name="upload-debtxclient")
     */
    public function generateDebtxclient(Request $request)
    {
        return $this->render('upload_debtxclient.html.twig');
    }

    /**
     * @Route("/plantCPE", name="plantCPE")
     */
    public function downloadPlant()
    {
        if (isset($_POST['boton1'])) {
            $encabezado = ['Numero de contrato', 'Nombre del contrato', 'Direccion', 'Localidad', 'Codigo Postal', 'Provincia', 'Telefono de contacto 1', 'Telefono de contacto 2', 'Email', 'Numero de referencia', 'Importe 1', 'Vencimiento 1', 'Importe 2', 'Vencimiento 2', 'Sucursal', 'Observaciones', 'CPE'];

            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooterDrawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath('../public/images/argon.png');
            $drawing->setHeight(85);

            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);

            $HojaCalculo = $spreadsheet->getActiveSheet();
            $HojaCalculo->setCellValue('A1', ' ')->mergeCells('A1:A4');
            $HojaCalculo->setCellValue('B1', ' ')->mergeCells('B1:Q4');
            $HojaCalculo->fromArray($encabezado, null, 'A5');
            $HojaCalculo->getStyle('B' . '1')->getAlignment()->setHorizontal(Alignment::VERTICAL_TOP);
            $HojaCalculo->getCell('B1')->setValue('Generador de Cartas');
            $HojaCalculo->getStyle('B1')->getFont()->setBold(true)->setSize(40);

            $drawing->setWorksheet($spreadsheet->getActiveSheet());
            $spreadsheet->getDefaultStyle()->getFont()->setSize(12);
            $spreadsheet->getActiveSheet()->setAutoFilter('A5:Q5');
            // @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet
            $spreadsheet->getActiveSheet()->getStyle('A5:Q5')
                ->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

            $spreadsheet->getActiveSheet()->getStyle('A5:Q5')
                ->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

            $spreadsheet->getActiveSheet()->getStyle('A5:Q5')
                ->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

            $spreadsheet->getActiveSheet()->getStyle('A5:Q5')
                ->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

            $spreadsheet->getActiveSheet()->getStyle('A5:Q5')
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
            $spreadsheet->getActiveSheet()->getStyle('A5:Q5')
                ->getFill()->getStartColor()->setARGB('DDDFD7');

            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->getActiveSheet()->getHeaderFooter()->addImage($drawing, \PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooter::IMAGE_HEADER_LEFT);

            $spreadsheet->getActiveSheet()->setBreak('D10', \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::BREAK_COLUMN);
            $sheet->setTitle('Plantilla_CPE');

            // Create your Office 2007 Excel (XLSX Format)
            $writer = new Xlsx($spreadsheet);

            $columnNames = [
                'Numero de contrato',
                'Nombre del contrato',
                'Direccion',
                'Localidad',
                'Codigo Postal',
                'Provincia',
                'Telefono de contacto 1',
                'Telefono de contacto 2',
                'Email',
                'Numero de referencia',
                'Importe 1',
                'Vencimiento 1',
                'Importe 2',
                'Vencimiento 2',
                'Sucursal',
                'Observaciones',
                'CPE',
            ];
            $columnLetter = 'A';

            $columnLetter = 'A';
            foreach ($columnNames as $columnName) {
                // Center text
                $sheet->getStyle($columnLetter . '1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle($columnLetter . '2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                // Text in bold
                $sheet->getStyle($columnLetter . '1')->getFont()->setBold(true);
                $sheet->getStyle($columnLetter . '2')->getFont()->setBold(true);
                // Autosize column
                $sheet->getColumnDimension($columnLetter)->setAutoSize(true);
                ++$columnLetter;
            }

            $em = $this->getDoctrine()->getEntityManager();

            $query1 = 'SELECT c.customer_reference, ANY_VALUE(c.name) AS name, '
                    . 'ANY_VALUE(c.address) AS address, ANY_VALUE(c.location) AS location, '
                    . 'ANY_VALUE(c.postal_code) AS postal_code, ANY_VALUE(c.province) AS province, '
                    . 'ANY_VALUE(c.contact_number) AS contact_number, '
                    . 'ANY_VALUE(c.contact_number2) AS contact_number2, ANY_VALUE(c.email) AS email, '
                    . 'ANY_VALUE(d.reference_number) AS reference_number, ROUND(SUM(d.amount),2) AS amount, '
                    . 'ANY_VALUE(d.expiration) AS expiration, ROUND(SUM(d.amount2),2) AS amount2, '
                    . 'ANY_VALUE(d.expiration2) AS expiration2, ANY_VALUE(d.branch_office) AS branch_office, '
                    . 'ANY_VALUE(d.observations) AS observations, d.code_pe '
                    . 'FROM vep_mg.debt d '
                    . 'LEFT JOIN vep_mg.customer c on c.id = d.customer_reference_id '
                    . 'GROUP BY c.customer_reference, d.code_pe '
                    . 'ORDER BY code_pe ASC';

            $statement = $em->getConnection()->prepare($query1);
            $statement->execute();

            $result = $statement->fetchAll();

            $array = $result;

            $numeroDeFila = 6;

            foreach ($array as $element) {
                $HojaCalculo->setCellValueByColumnAndRow(1, $numeroDeFila, $element['customer_reference']);
                $HojaCalculo->setCellValueByColumnAndRow(2, $numeroDeFila, $element['name']);
                $HojaCalculo->setCellValueByColumnAndRow(3, $numeroDeFila, $element['address']);
                $HojaCalculo->setCellValueByColumnAndRow(4, $numeroDeFila, $element['location']);
                $HojaCalculo->setCellValueByColumnAndRow(5, $numeroDeFila, $element['postal_code']);
                $HojaCalculo->setCellValueByColumnAndRow(6, $numeroDeFila, $element['province']);
                $HojaCalculo->setCellValueByColumnAndRow(7, $numeroDeFila, $element['contact_number']);
                $HojaCalculo->setCellValueByColumnAndRow(8, $numeroDeFila, $element['contact_number2']);
                $HojaCalculo->setCellValueByColumnAndRow(9, $numeroDeFila, $element['email']);
                $HojaCalculo->setCellValueByColumnAndRow(10, $numeroDeFila, $element['reference_number']);
                $HojaCalculo->setCellValueByColumnAndRow(11, $numeroDeFila, $element['amount']);
                $HojaCalculo->setCellValueByColumnAndRow(12, $numeroDeFila, $element['expiration']);
                $HojaCalculo->setCellValueByColumnAndRow(13, $numeroDeFila, $element['amount2']);
                $HojaCalculo->setCellValueByColumnAndRow(14, $numeroDeFila, $element['expiration2']);
                $HojaCalculo->setCellValueByColumnAndRow(15, $numeroDeFila, $element['branch_office']);
                $HojaCalculo->setCellValueByColumnAndRow(16, $numeroDeFila, $element['observations']);
                $HojaCalculo->setCellValueByColumnAndRow(17, $numeroDeFila, $element['code_pe']);
                ++$numeroDeFila;
            }

            /*
            foreach ($array as $name => $value) {

                $name1=$Obj->reference_number;
                $name2=$Obj->name;
                $name3=$Obj->address;
                $name4=$Obj->location;
                $name5=$Obj->postal_code;
                $name6=$Obj->province;
                $name7=$Obj->contact_number;
                $name8=$Obj->contact_number2;
                $name9=$Obj->email;
                $name10=$Obj->reference_number;
                $name11=$Obj->amount;
                $name12=$Obj->expiration;
                $name13=$Obj->amount2;
                $name14=$Obj->expiration2;
                $name15=$Obj->branch_office;
                $name16=$Obj->observations;
                $name17=$Obj->code_pe;
                $name18=$Obj->created_at;
                $name19=$Obj->status;
            }
            */

            /*
             $query = $em->createQuery('INSERT u FROM App:Debtxclient u JOIN App:Debt b WITH u.id = b.id JOIN App:Customer h WITH u.id = h.id');
             $articulos=$query->getResult(Query::HYDRATE_OBJECT);
            */

            // Create a Temporary file in the system
            $fileName = 'Plantilla_CPE.xlsx';
            $temp_file = tempnam(sys_get_temp_dir(), $fileName);

            // Create the excel file in the tmp directory of the system
            $writer->save($temp_file);

            // Return the excel file as an attachment
            return $this->file($temp_file, $fileName);
        }

        return $this->render('upload_debtxclient.twig');
    }

    /**
     * @Route("/historico", name="reporteador")
     */
    public function historico()
    {
        $em = $this->getDoctrine()->getManager();
        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = [
            'url' => 'mysql://root:root@simp-db:3306/simp',
        ];

        $db = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        /*$query = "SELECT db.batch_date, db.batch_number, d.reference_number, c.name, "
            . "d.amount1, d.expiration_date1 "
            . "FROM simp.debt d "
            . "left join client c on c.client_id = d.client_id "
            . "left join debt_batch db on db.debt_batch_id = d.debt_batch_id "
            . "where c.company_id = 16 AND d.reference_number like '990%' ";
        $stmt = $db->prepare($query);
        $params = [];
        $stmt->execute($params);
        $arr = $stmt->fetchAll();

        $nr = count($arr);
        //echo $nr;
        //print $arr;
        $arrCol = ['Fecha del lote', 'Vencimiento1', 'Numero de Referencia', 'Importe1', 'Nombre', 'Numero de lote'];
        $nc = count($arrCol);*/

        $queryp = "SELECT c.name, p.amount, p.payment_date, c.psf_code, "
                . "e.name as entity "
                . "FROM simp.payment p "
                . "LEFT JOIN simp.client c on c.client_id = p.client_id "
                . "LEFT JOIN simp.payment_method pm on pm.payment_method_id = p.payment_method_id "
                . "LEFT JOIN simp.entity e on e.entity_id = pm.entity_id "
                . "WHERE c.company_id = 16 AND c.psf_code like '990%' ";
        $stmtp = $db->prepare($queryp);
        $paramsp = [];
        $stmtp->execute($paramsp);
        $pagp = $stmtp->fetchAll();

        $nrp = count($pagp);
        //echo $nr;
        //print $arr;
        $pagCol = ['Número de Contrato', 'CPE', 'Monto', 'Fecha de pago', 'Medio de pago'];
        $ncp = count($pagCol);

        return $this->render('historico.html.twig', ['pagp' => $pagp, 'pagCol' => $pagCol]);
    }
}
