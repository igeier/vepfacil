<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @ORM\OneToMany(targetEntity="Debt", mappedBy="customerReferenceId")
     */
    private $customerReference;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $postalCode;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $province;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $contactNumber;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $contactNumber2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomerReference(): ?string
    {
        return $this->customerReference;
    }

    public function setCustomerReference(string $customerReference): self
    {
        $this->customerReference = $customerReference;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
    
    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }
    
    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }
    
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }
    
    public function getProvince(): ?string
    {
        return $this->province;
    }

    public function setProvince(?string $province): self
    {
        $this->province = $province;

        return $this;
    }
    
    public function getContactNumber(): ?string
    {
        return $this->contactNumber;
    }

    public function setContactNumber(?string $contactNumber): self
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }
    
    public function getContactNumber2(): ?string
    {
        return $this->contactNumber2;
    }

    public function setContactNumber2(?string $contactNumber2): self
    {
        $this->contactNumber2 = $contactNumber2;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
    
    public function __construct() {
        $this->customerReference = new ArrayCollection();
    }
    
    /*public function addDebtGroup(DebtGroup $debtGroup): self
    {
        if (!$this->debtGroups->contains($debtGroup)) {
            $this->debtGroups[] = $debtGroup;
            $debtGroup->setCustomer($this);
        }

        return $this;
    }

    public function removeDebtGroup(DebtGroup $debtGroup): self
    {
        if ($this->debtGroups->contains($debtGroup)) {
            $this->debtGroups->removeElement($debtGroup);
            // set the owning side to null (unless already changed)
            if ($debtGroup->getCustomer() === $this) {
                $debtGroup->setCustomer(null);
            }
        }

        return $this;
    }*/
}
