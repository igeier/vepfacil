<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DebtRepository")
 */
class Debt
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="customerReference")
     * @ORM\JoinColumn(name="customer_reference_id", referencedColumnName="id")
     */
    private $customerReferenceId;
    
    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $referenceNumber;
    
    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private $amount;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $expiration;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount2;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiration2;
    
    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $branchOffice;
    
    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $observations;
    
    /**
     * @ORM\Column(type="string", length=19, nullable=false)
     */
    private $codePe;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\ManyToOne(targetEntity="App\Entity\Status", inversedBy="name")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $statusId;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $sent;
    

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getCustomerReferenceId(): ?int
    {
        return $this->customerReferenceId;
    }

    public function setCustomerReferenceId(int $customerReferenceId): self
    {
        $this->customerReferenceId = $customerReferenceId;

        return $this;
    }
    
    public function getReferenceNumber(): ?string
    {
        return $this->referenceNumber;
    }
    
    public function setReferenceNumber(?string $referenceNumber): self
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getExpiration(): ?\DateTimeInterface
    {
        return $this->expiration;
    }

    public function setExpiration(\DateTimeInterface $expiration): self
    {
        $this->expiration = $expiration;

        return $this;
    }
    
    public function getAmount2(): ?float
    {
        return $this->amount2;
    }

    public function setAmount2(float $amount2): self
    {
        $this->amount2 = $amount2;

        return $this;
    }

    public function getExpiration2(): ?\DateTimeInterface
    {
        return $this->expiration2;
    }

    public function setExpiration2(\DateTimeInterface $expiration2): self
    {
        $this->expiration2 = $expiration2;

        return $this;
    }
    
    public function getBranchOffice(): ?string
    {
        return $this->branchOffice;
    }
    
    public function setBranchOffice(?string $branchOffice): self
    {
        $this->branchOffice = $branchOffice;

        return $this;
    }
    
    public function getObservations(): ?string
    {
        return $this->Observations;
    }
    
    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }
    
    public function getCodePe(): ?string
    {
        return $this->codePe;
    }
    
    public function setCodePe(?string $codePe): self
    {
        $this->codePe = $codePe;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatusId(): ?int
    {
        return $this->statusId;
    }

    public function setStatusId(int $statusId): self
    {
        $this->statusId = $statusId;

        return $this;
    }
    
    public function getSent(): ?int
    {
        return $this->sent;
    }

    public function setSent(int $sent): self
    {
        $this->sent = $sent;

        return $this;
    }
}
